﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sql_Database
{
    public class CustomerGenre
    {
        public int CustomerId { get; set; }
        public string Genre { get; set; }
        public int Count { get; set; }

    }
}
