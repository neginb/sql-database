﻿using Sql_Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sql_Database.Repositories
{
    public interface ICustomerCountryRepository
    {
        public List<CustomerCountry> GetNumberOfCustomers();
    }
}
