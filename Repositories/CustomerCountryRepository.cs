﻿using Microsoft.Data.SqlClient;
using Sql_Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sql_Database.Repositories
{
   public class CustomerCountryRepository : ICustomerCountryRepository
    {
        public List<CustomerCountry> GetNumberOfCustomers()
        {
            List<CustomerCountry> res = new List<CustomerCountry>();

            string sql = "SELECT COUNT(CustomerId) As NumberOfCustomer, Country FROM Customer Group By Country ORDER BY NumberOfCustomer DESC";

            try
            {
                //Connect 
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry customer = new CustomerCountry();
                                customer.NumberOfCustomer = reader.GetInt32(0);
                                customer.Country = reader["Country"] != DBNull.Value ? reader.GetString(1).ToString() : string.Empty;
                                res.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log
            }
            return res;
        }
    }
}
