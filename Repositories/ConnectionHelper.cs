﻿using Microsoft.Data.SqlClient;

namespace Sql_Database.Repositories
{
    public class ConnectionHelper
    {
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.DataSource = @"N-SE-01-9462\SQLEXPRESS"; // @"N-SE-01-4753\SQLEXPRESS";
            connectionStringBuilder.InitialCatalog = "Chinook";
            connectionStringBuilder.IntegratedSecurity = true;
            connectionStringBuilder.Encrypt = false;
            return connectionStringBuilder.ConnectionString;

        }
    }
}
