﻿
using Microsoft.Data.SqlClient;
using Sql_Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sql_Database.Repositories

{
    public class CustomerRepository : ICustomerRepository
    {

        public List<Customer> GetAllCustomers()
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                //Connect 
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader["Country"] != DBNull.Value ? reader.GetString(3).ToString() : string.Empty;
                                temp.PostalCode = reader["PostalCode"] != DBNull.Value ? reader.GetString(4).ToString() : string.Empty;
                                temp.Phone = reader["Phone"] != DBNull.Value ? reader.GetString(5).ToString() : string.Empty;
                                temp.Email = reader.GetString(6);
                                custList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {


            }
            return custList;
        }
        public Customer GetCustomer(int id)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                " WHERE CustomerId = @CustomerId";

            try
            {
                //Connect 
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader["Country"] != DBNull.Value ? reader.GetString(3).ToString() : string.Empty;
                                customer.PostalCode = reader["PostalCode"] != DBNull.Value ? reader.GetString(4).ToString() : string.Empty;
                                customer.Phone = reader["Phone"] != DBNull.Value ? reader.GetString(5).ToString() : string.Empty;
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log
            }
            return customer;
        }

        public bool AddNewCustomer(Customer customer)
        {
   bool success = false; string sql = " INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email)" + "VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)"; try { using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString())) { conn.Open(); using (SqlCommand cmd = new SqlCommand(sql, conn)) { cmd.Parameters.AddWithValue("@FirstName", customer.FirstName); cmd.Parameters.AddWithValue("@LastName", customer.LastName); cmd.Parameters.AddWithValue("@Country", customer.Country); cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode); cmd.Parameters.AddWithValue("@Phone", customer.Phone); cmd.Parameters.AddWithValue("@Email", customer.Email); success = cmd.ExecuteNonQuery() > 0 ? true : false; } } }
            catch (Exception ex)
            {                //Log
            }
            return success;
        }



        public bool UpdateCustomer(Customer customer, int id)
        {
 bool success = false; string sql = "UPDATE Customer " + "SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email " + "WHERE CustomerId = @CustomerId"; try { using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString())) { conn.Open(); using (SqlCommand cmd = new SqlCommand(sql, conn)) { cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId); cmd.Parameters.AddWithValue("@FirstName", customer.FirstName); cmd.Parameters.AddWithValue("@LastName", customer.LastName); cmd.Parameters.AddWithValue("@Country", customer.Country); cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode); cmd.Parameters.AddWithValue("@Phone", customer.Phone); cmd.Parameters.AddWithValue("@Email", customer.Email); success = cmd.ExecuteNonQuery() > 0 ? true : false; } } }
            catch (Exception ex)
            {                //Log
            }
            return success;
        }

        public List<Customer> GetCustomerByName(string name)
        {
            List<Customer> res = new List<Customer>();
         
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                " WHERE FirstName = @FirstName ORDER BY CustomerId Asc";
 
            try
            {
                //Connect 
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", name);
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            
                            while (reader.Read())
                            {
                                // Handle result
                                Customer customer = new Customer();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader["Country"] != DBNull.Value ? reader.GetString(3).ToString() : string.Empty;
                                customer.PostalCode = reader["PostalCode"] != DBNull.Value ? reader.GetString(4).ToString() : string.Empty;
                                customer.Phone = reader["Phone"] != DBNull.Value ? reader.GetString(5).ToString() : string.Empty;
                                customer.Email = reader.GetString(6);
                                res.Add(customer);
                            }
                        }
                    }
                }

            }

            catch (SqlException ex)
            {
                // Log
            }
            return res;
        }

        public List<Customer> GetCustomerByLimitAndOffset(int limit, int offset)
        {
            List<Customer> res = new List<Customer>();

            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY";

            try
            {
                //Connect 
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@offset", offset);
                        cmd.Parameters.AddWithValue("@limit", limit);
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                // Handle result
                                Customer customer = new Customer();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader["Country"] != DBNull.Value ? reader.GetString(3).ToString() : string.Empty;
                                customer.PostalCode = reader["PostalCode"] != DBNull.Value ? reader.GetString(4).ToString() : string.Empty;
                                customer.Phone = reader["Phone"] != DBNull.Value ? reader.GetString(5).ToString() : string.Empty;
                                customer.Email = reader.GetString(6);
                                res.Add(customer);
                            }
                        }
                    }
                }

            }

            catch (SqlException ex)
            {
                // Log
            }
            return res;
        }
    }
}

