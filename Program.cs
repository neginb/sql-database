﻿using Sql_Database.Models;
using Sql_Database.Repositories;
using System;
using System.Collections.Generic;

namespace Sql_Database
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            ICustomerCountryRepository repository = new CustomerCountryRepository();
            TestGetNumberOfCustomers(repository);

        }

        static void TestSelectAll(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }
        static void TestSelect(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomer(1));
        }

        static void TestInsert(ICustomerRepository repository)
        {
            Customer test = new Customer()
            {
                FirstName = "Diego",
                LastName = "Peterson",
                Country = "Sweden",
                PostalCode = "1234",
                Phone = "12334567",
                Email = "fadi@email.com"
            };
            if (repository.AddNewCustomer(test))
            {
                Console.WriteLine("Added a new customer!");
            }
            else
            {
                Console.WriteLine("could not add a new customer!");
            }
        }
        static void TestUpdate(ICustomerRepository repository, int id) { Customer test = new Customer() { CustomerId = id, FirstName = "Nogin", LastName = "Peterson", Country = "Lebanan", PostalCode = "1234", Phone = "12334567", Email = "fadi@email.com" }; if (repository.UpdateCustomer(test, id)) { Console.WriteLine("Updated customer!"); PrintCustomer(repository.GetCustomer(60)); } else { Console.WriteLine("could not update the customer!"); } }
        static void TestGetByName(ICustomerRepository repository, string firstName)
        {
            PrintCustomers(repository.GetCustomerByName(firstName));
        }

        static void PrintCustomerCountry(IEnumerable<CustomerCountry> customers)
        {
            foreach (CustomerCountry customer in customers)
            {
                Console.WriteLine($"--- {customer.Country}{customer.NumberOfCustomer} ---");
            }
        }
        //static void PrintCustomerC(CustomerCountry customer)
        //{
        //    Console.WriteLine($"--- {customer.Country}{customer.NumberOfCustomer} ---");
        //}

        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"---{customer.CustomerId} {customer.FirstName} {customer.LastName} {customer.Country} {customer.PostalCode} {customer.Phone} {customer.Email} ---");
        }

        static void TestGetCustomerByLimitAndOffset(ICustomerRepository repository, int limit, int offset)
        {
            PrintCustomers(repository.GetCustomerByLimitAndOffset(limit, offset));
        }

        static void TestGetNumberOfCustomers(ICustomerCountryRepository repository)
        {
            PrintCustomerCountry(repository.GetNumberOfCustomers());
        }
    }
}
